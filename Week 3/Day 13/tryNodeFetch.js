const fetch = require("node-fetch");

const url = "http://fakeapi.jsonparseronline.com/categories/1";
const url2 = "https://jsonplaceholder.typicode.com/albums/60";
const url3 = "https://myfakeapi.com/api/cars/1";

/* Promise */
/* fetch(url)
	.then((res) => res.json())
	.then((json) => console.log(json));
fetch(url2)
	.then((res) => res.json())
	.then((json) => console.log(json));
fetch(url3)
	.then((res) => res.json())
	.then((json) => console.log(json)); */

/* Async await */
async function fetchAPI() {
	try {
		response = await fetch(url);
		data = await response.json();
		console.log(data);

		response = await fetch(url2);
		data = await response.json();
		console.log(data);

		response = await fetch(url3);
		data = await response.json();
		console.log(data);
	} catch (error) {
		throw error;
	}
}
fetchAPI();
