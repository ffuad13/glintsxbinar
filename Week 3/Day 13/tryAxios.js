const axios = require("axios");

const url = "http://fakeapi.jsonparseronline.com/categories/1";
const url2 = "https://jsonplaceholder.typicode.com/albums/60";
const url3 = "https://myfakeapi.com/api/cars/1";

/* Promise */
/* axios.get(url).then((response) => {
	console.log(response.data);
});

axios.get(url2).then((response) => {
	console.log(response.data);
});

axios.get(url3).then((response) => {
	console.log(response.data);
}); */

/* Async await */
async function fetchAPI() {
	try {
		response = await axios.get(url);
		console.log(response.data);

		response = await axios.get(url2);
		console.log(response.data);

		response = await axios.get(url3);
		console.log(response.data);
	} catch (error) {
		throw error;
	}
}

fetchAPI();
