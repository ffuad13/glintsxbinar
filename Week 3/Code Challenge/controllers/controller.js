const pokemon = require("../models/pokeDB.json");

class controller {
	getAllPokemon(req, res) {
		try {
			res.status(200).json({
				content: pokemon,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	getPokemon(req, res) {
		try {
			const poke = pokemon.filter((poke) => poke.id === eval(req.params.id));

			res.status(200).json({
				content: poke,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	makePokemon(req, res) {
		try {
			pokemon.push(req.body);

			res.status(200).json({
				content: req.body,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	editPokemon(req, res) {
		try {
			const ids = eval(req.params.id);
			const edit = req.body;

			if (pokemon[ids] != null) {
				pokemon[id] = edit;
			}

			res.status(201).json({
				content: edit,
				msg: `Pokemon data has been updated`,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}

	removePokemon(req, res) {
		try {
			const ids = eval(req.params.id);

			pokemon.filter((poke) => {
				return poke.id != ids;
			});

			res.status(201).json({
				content: `The pokemon number ${ids} has been deleted`,
			});
		} catch (error) {
			res.status(500).json({
				message: error.message,
			});
		}
	}
}

module.exports = new controller();
