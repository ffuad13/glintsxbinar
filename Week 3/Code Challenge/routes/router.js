const express = require("express");

const router = express.Router();

const { getAllPokemon, getPokemon, makePokemon, editPokemon, removePokemon } = require("../controllers/controller");

/* CREATE data */
router.post("/", makePokemon);

/* READ data */
router.get("/", getAllPokemon);
router.get("/:id", getPokemon);

/* UPDATE data */
router.put("/:id", editPokemon);

/* DELETE data */
router.delete("/:id", removePokemon);

module.exports = router;
