const express = require("express");

const app = express();

/* enable req.body */
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

/* import routes */
const router = require("./routes/router");

app.use("/", router);

app.listen(3000, () => console.log(`Server is running on port 3000`));
