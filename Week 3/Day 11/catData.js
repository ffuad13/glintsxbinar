const Cat = require("./cat");

const cat1 = new Cat("Lily", 2, "Persia");
const cat2 = new Cat("Molly", 1, "Angora");
const cat3 = new Cat("Garfield", 1, "Actor");
const cat4 = new Cat("Empus", 3, "Kampong");
const cat5 = new Cat("Oren", 5, "Street Cat");

console.log("---------");
cat4.info();
console.log("---------");
cat5.info();
