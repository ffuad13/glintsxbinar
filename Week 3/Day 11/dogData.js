const Dog = require("./dog");

const dog1 = new Dog("Leo", 4, "Chihuahua");
const dog2 = new Dog("Ben", 2, "Poodle");
const dog3 = new Dog("Kelly", 1, "Bulldog");
const dog4 = new Dog("Archy", 5, "Corgi");
const dog5 = new Dog("Manny", 6, "Shepard");

console.log("---------");
dog1.info();
console.log("---------");
dog2.info();
