/* Perimeter of Triangle */
function calculatePerimeter(ab, bc, ca) {
  let side1 = ab;
  let side2 = bc;
  let side3 = ca;
  return side1 + side2 + side3;
}

let result = calculatePerimeter(6, 6, 6);
console.log(`The perimeter of triangle is ${result} cm\n`);

/* Volume of Beam */
function caclulateVolume(length, width, height) {
  let l = length;
  let w = width;
  let h = height;
  return l * w * h;
}

let result2 = caclulateVolume(13, 8, 6);
console.log(`Volume of beam is ${result2} cm³\n`);

/* Volume of Tube */
function tubeVolume(a, b) {
  let pi = 3.14;
  let r = a / 2;
  let r2 = r ** 2;
  let h = b;
  return pi * r2 * h;
}

let result3 = tubeVolume(50, 66);
console.log(`Volume of tube is ${result3} cm³`);
