const login = require("../Day 10/login");

let patient = [
	{
		name: "Robert",
		status: "Suspect",
	},
	{
		name: "Evelyn",
		status: "Negative",
	},
	{
		name: "Zack",
		status: "Negative",
	},
	{
		name: "Aaron",
		status: "Suspect",
	},
	{
		name: "Jake",
		status: "Positive",
	},
	{
		name: "Kyle",
		status: "Positive",
	},
];

function check() {
	console.log(`Check`);
	console.log(`=====`);
	console.log(`1. Negative`);
	console.log(`2. Positive`);
	console.log(`3. Suspect`);
	console.log(`4. Exit`);
	login.rl.question("Choose option: ", (status) => {
		switch (eval(status)) {
			case 1:
				console.log(`\n${patient[1].name} status is ${patient[1].status}`);
				console.log(`${patient[2].name} status is ${patient[2].status}\n`);
				check();
				break;
			case 2:
				console.log(`\n${patient[4].name} status is ${patient[4].status}`);
				console.log(`${patient[5].name} status is ${patient[5].status}\n`);
				check();
				break;
			case 3:
				console.log(`\n${patient[0].name} status is ${patient[0].status}`);
				console.log(`${patient[3].name} status is ${patient[3].status}\n`);
				check();
				break;
			case 4:
				login.rl.close();
				break;
			default:
				console.log(`\nNo status recorded\n`);
				check();
				break;
		}
	});
}

module.exports = { check };
