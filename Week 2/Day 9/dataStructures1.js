let baskets = ["tomato", "broccoli", "kale", "cabbage", "apple"];

baskets.pop();
baskets.forEach((basket) => {
	console.log(`${basket} is a healthy food, it's definitely worth to eat.`);
});
