const EventEmitter = require("events");
const readline = require("readline");

const dataStructures2 = require("../Day 9/dataStructures2");

const eve = new EventEmitter();
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

// Registering a listener
eve.on("Login Failed", function (email) {
	console.log(`${email}, is failed to login!`);
	rl.close();
});

eve.on("Login Success", function (email) {
	console.log(`${email}, is success to login!\n`);
	dataStructures2.check(); // change to assign2 funct
});

const user = {
	login(email, password) {
		const passwordStoredInDatabase = "123456";

		if (password !== passwordStoredInDatabase) {
			eve.emit("Login Failed", email); // Pass the email to the listener
		} else {
			eve.emit("Login Success", email);
		}
	},
};

rl.question("Email: ", function (email) {
	rl.question("Password: ", (password) => {
		user.login(email, password); // Run login function
	});
});

module.exports.rl = rl;
