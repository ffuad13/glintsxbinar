const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
	return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
	// Code Here
	let l = data.length;
	let isSwap = false;
	do {
		isSwap = false;
		for (let i = 0; i < l; i++) {
			if (data[i] > data[i + 1]) {
				let tmp = data[i];
				data[i] = data[i + 1];
				data[i + 1] = tmp;
				isSwap = true;
			}
		}
	} while (isSwap);
	return clean(data);
}

// Should return array
function sortDescending(data) {
	// Code Here
	let l = data.length;
	let isSwap;
	do {
		isSwap = false;
		for (let i = 0; i < l; i++) {
			if (data[i] < data[i + 1]) {
				let tmp = data[i];
				data[i] = data[i + 1];
				data[i + 1] = tmp;
				isSwap = true;
			}
		}
	} while (isSwap);
	return clean(data);
}

// DON'T CHANGE
test(sortAscending, sortDescending, data);
