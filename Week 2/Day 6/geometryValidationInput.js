/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

// Function to calculate beam volume
function beamVolume(length, width, height) {
	return length * width * height;
}

// Function to calculate tube Volume
function tubeVolume(diameter, height) {
	let radius = diameter / 2;
	let r2 = radius ** radius;
	return (Math.PI * r2 * height).toFixed(2);
}

// function to calculate triangle perimeter
function trianglePerimeter(sideA, sideB, sideC) {
	return sideA * 1 + sideB * 1 + sideC * 1;
}

/* Alternative Way */
// All input just in one code
function input() {
	rl.question("Length: ", function (length) {
		rl.question("Width: ", (width) => {
			rl.question("Height: ", (height) => {
				if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
					console.log(`\nBeam: ${beamVolume(length, width, height)}\n`);
					input2();
				} else {
					console.log(`Length, Width and Height must be a number\n`);
					input();
				}
			});
		});
	});
}

function input2() {
	console.log("Tube Volume");
	console.log("=========");
	rl.question("Diameter: ", function (diameter) {
		rl.question("Height: ", (height) => {
			if (!isNaN(diameter) && !isNaN(height)) {
				console.log(`\nTube: ${tubeVolume(diameter, height)}\n`);
				input3();
			} else {
				console.log(`Diameter and Height must be a number\n`);
				input2();
			}
		});
	});
}

function input3() {
	console.log("Perimeter of Triangle");
	console.log("============");
	rl.question("Side A: ", function (sideA) {
		rl.question("Side B: ", (sideB) => {
			rl.question("Side C: ", (sideC) => {
				if (!isNaN(sideA) && !isNaN(sideB) && !isNaN(sideC)) {
					console.log(`\nTriangle: ${trianglePerimeter(sideA, sideB, sideC)}\n`);
					rl.close();
				} else {
					console.log(`All Side must be a number\n`);
					input3();
				}
			});
		});
	});
}
/* End Alternative Way */

console.log(`Beam Volume`);
console.log(`=========`);
input(); // Call Alternative Way
